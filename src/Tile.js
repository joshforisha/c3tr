import * as Avatar from '/components/Avatar'
import * as BackgroundColor from '/components/BackgroundColor'
import TileType from '/data/TileType'

const Value = Object.assign({
  [TileType.Cave]: {
    avatar: Avatar.create('•', '#504030').value,
    backgroundColor: BackgroundColor.create('#4A3A2A').value
  },

  [TileType.Ice]: {
    avatar: Avatar.create('#', '#9BD').value,
    backgroundColor: BackgroundColor.create('#8AC').value
  },

  [TileType.Land]: {
    avatar: Avatar.create('•', '#2A4C08').value,
    backgroundColor: BackgroundColor.create('#240').value
  },

  [TileType.Lava]: {
    avatar: Avatar.create('~', '#B55').value,
    backgroundColor: BackgroundColor.create('#A44').value
  },

  [TileType.Mountain]: {
    avatar: Avatar.create('×', '#505050').value,
    backgroundColor: BackgroundColor.create('#444').value
  },

  [TileType.Water]: {
    avatar: Avatar.create('~', '#357').value,
    backgroundColor: BackgroundColor.create('#246').value
  }
})

export function generateGrid (columns, rows, typeFn) {
  const tiles = new Array(columns)

  for (let x = 0; x < columns; x++) {
    tiles[x] = new Uint8Array(rows)
    for (let y = 0; y < rows; y++) {
      tiles[x][y] = typeFn(x, y)
    }
  }

  return tiles
}

export function is (tile, tileType) {
  return tile === tileType
}

export function isLand (tile) {
  return is(tile, TileType.Land)
}

export function isPassable (tile) {
  return !is(tile, TileType.Lava) && !is(tile, TileType.Mountain)
}

export function typeString (tile) {
  switch (tile) {
    case TileType.Cave:
      return 'Cave'
    case TileType.Ice:
      return 'Ice'
    case TileType.Land:
      return 'Land'
    case TileType.Lava:
      return 'Lava'
    case TileType.Mountain:
      return 'Mountain'
    case TileType.Water:
      return 'Water'
    default:
      return ''
  }
}

export function value (tile) {
  return Value[tile]
}
