import * as Battery from '/entities/Battery'
import * as CPU from '/entities/CPU'
import * as Monster from '/entities/Monster'
import * as Random from '/Random'
import * as Tile from '/Tile'
import * as Tree from '/entities/Tree'
import * as World from '/World'
import FSN from 'fast-simplex-noise'
import TileType from '/data/TileType'
import { pipe } from '/Util'

export function generate (width, height) {
  const caveGen = new FSN({ frequency: 0.05, octaves: 6 })
  const mountainGen = new FSN({ frequency: 0.01, octaves: 12 })
  const terrainGen = new FSN({ frequency: 0.01, octaves: 8 })

  const cave = (x, y) => Math.abs(caveGen.cylindrical2D(width, x, y))
  const mountain = (x, y) => mountainGen.cylindrical2D(width, x, y)
  const terrain = (x, y) => terrainGen.cylindrical2D(width, x, y)

  const tiles = Tile.generateGrid(width, height, (x, y) => {
    const t = terrain(x, y)

    if (t > 0.15) {
      if (t > 0.3 && mountain(x, y) > 0.3) {
        return cave(x, y) < 0.1 ? TileType.Cave : TileType.Mountain
      }

      return TileType.Land
    }

    return TileType.Water
  })

  return {
    height,
    tiles,
    width
  }
}

export function populate (world) {
  const place = (entity, count) => entities =>
    World.placeEntity(world, entities, [TileType.Land], entity, count)

  return pipe(
    [],
    [
      place(Battery.create, Random.int(150, 250)),
      place(CPU.create, Random.int(10, 50)),
      place(Monster.create, Random.int(1000, 2000)),
      place(Tree.create, Random.int(3000, 4000))
    ]
  )
}

export function startingPosition (world, entities) {
  return World.randomEmptyPosition(world, entities, [TileType.Land])
}
