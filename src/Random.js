const almostOne = 1 - 1e-16 // The highest float less than 1

export function character () {
  return from(characters)
}

export function float (min = 0, max = Number.MAX_VALUE) {
  return Math.random() * (max - min) + min
}

export function from (enumerable) {
  if (Array.isArray(enumerable)) {
    return enumerable[int(0, enumerable.length - 1)]
  }

  return enumerable[from(Object.keys(enumerable))]
}

export function int (min = 0, max = Number.MAX_VALUE) {
  return Math.floor(Math.random() * (max - min + almostOne) + min)
}

const characters = [
  '0',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P',
  'Q',
  'R',
  'S',
  'T',
  'U',
  'V',
  'W',
  'X',
  'Y',
  'Z',
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'l',
  'm',
  'n',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'w',
  'x',
  'y',
  'z'
]
