import Action from '/data/Action'
import Key from '/data/Key'

export default [
  { key: Key.Escape, action: Action.FocusGame },
  { key: Key.LowerI, action: Action.FocusGame }
]
