import Action from '/data/Action'
import Key from '/data/Key'

export default [
  { key: Key.Comma, action: Action.FocusGame },
  { key: Key.Escape, action: Action.FocusGame },
  { key: Key.Enter, action: Action.TakeItem },
  { key: Key.LowerB, action: Action.SelectNextItem },
  { key: Key.LowerK, action: Action.SelectPreviousItem }
]
