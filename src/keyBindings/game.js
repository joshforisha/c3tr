import Action from '/data/Action'
import Key from '/data/Key'

export default [
  { key: Key.Comma, action: Action.FocusItems },
  { key: Key.Down, action: Action.MoveSouth },
  { key: Key.Left, action: Action.MoveWest },
  { key: Key.LowerB, action: Action.MoveSoutwest },
  { key: Key.LowerH, action: Action.MoveSest },
  { key: Key.LowerI, action: Action.FocusInventory },
  { key: Key.LowerJ, action: Action.MoveSouth },
  { key: Key.LowerK, action: Action.MoveNorth },
  { key: Key.LowerL, action: Action.MoveEast },
  { key: Key.LowerM, action: Action.FocusLog },
  { key: Key.LowerN, action: Action.MoveSoutheast },
  { key: Key.LowerU, action: Action.MoveNortheast },
  { key: Key.LowerY, action: Action.MoveNorthwest },
  { key: Key.Right, action: Action.MoveEast },
  { key: Key.Up, action: Action.MoveNorth }
]
