import * as Carl from '/entities/Carl'
import * as DOM from '/DOM'
import * as Position from '/components/Position'
import * as Tile from '/Tile'
import * as World from '/World'
import { constrain } from '/Util'

const tileSize = 30

export default function renderField ({ state, world }) {
  const fieldCanvas = DOM.find('#Field')
  const fieldContext = fieldCanvas.getContext('2d')

  fieldContext.clearRect(0, 0, fieldCanvas.width, fieldCanvas.height)
  fieldContext.font = `${tileSize}px monospace`

  const carl = Carl.findIn(state.entities)
  const carlPos = carl.position

  const remX = fieldCanvas.width % tileSize
  const remY = fieldCanvas.height % tileSize
  const offsetX = remX > 0 ? remX - tileSize : 0
  const offsetY = remY > 0 ? remY - tileSize : 0

  const columns = Math.floor(fieldCanvas.width / tileSize)
  const rows = Math.floor(fieldCanvas.height / tileSize)

  const colSpan = Math.ceil(columns / 2)
  const rowSpan = Math.ceil(rows / 2)

  const minY = carlPos.y - rowSpan
  const maxY = carlPos.y + rowSpan

  let filterPos, maxX, minX
  if (carlPos.x + colSpan > world.width || carlPos.x < colSpan) {
    minX = constrain(carlPos.x - colSpan, 0, world.width)
    maxX = constrain(carlPos.x + colSpan, 0, world.width)
    filterPos = a =>
      (a.position.x >= minX &&
        a.position.x <= world.width &&
        a.position.y >= minY &&
        a.position.y <= maxY) ||
      (a.position.x >= 0 &&
        a.position.x <= maxX &&
        a.position.y >= minY &&
        a.position.y <= maxY)
  } else {
    minX = Math.max(carlPos.x - colSpan, 0)
    maxX = Math.min(carlPos.x + colSpan, world.width)
    filterPos = a =>
      a.position.x >= minX &&
      a.position.x <= maxX &&
      a.position.y >= minY &&
      a.position.y <= maxY
  }

  const visibleEntities = state.entities
    .filter(e => e.avatar && e.position)
    .filter(filterPos)

  const drawEntities = []

  for (let column = 0; column <= columns; column++) {
    for (let row = 0; row <= rows; row++) {
      const x = constrain(carlPos.x + column - colSpan, 0, world.width)
      const y = carlPos.y + row - rowSpan
      const i = tileSize * column + offsetX
      const j = tileSize * row + offsetY

      if (y < 0 || y >= world.height) continue

      const tile = World.tileAt(world, { x, y })
      const tileValue = Tile.value(tile)

      fieldContext.fillStyle = tileValue.backgroundColor
      fieldContext.fillRect(i, j, tileSize, tileSize)

      const presentEntities = visibleEntities.filter(e =>
        Position.match(e.position, { x, y })
      )

      if (presentEntities.length > 0) {
        const avatar = presentEntities.reduce(
          (top, entity) =>
            entity.avatar.importance > top.avatar.importance ? entity : top
        ).avatar
        drawEntities.push([avatar, i + 0.02 * tileSize, j + 0.88 * tileSize])
      } else {
        drawEntities.push([
          tileValue.avatar,
          i + 0.24 * tileSize,
          j + 0.8 * tileSize
        ])
      }
    }
  }

  drawEntities.forEach(([avatar, x, y]) => {
    fieldContext.fillStyle = avatar.style
    fieldContext.fillText(avatar.character, x, y)
  })
}
