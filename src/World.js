import * as Position from '/components/Position'
import * as Random from '/Random'
import { constrain } from '/Util'

export function placeEntity (
  world,
  entities,
  tileTypes,
  createEntity,
  count = 1
) {
  const total = entities.length + count

  while (entities.length < total) {
    entities = entities.concat(
      createEntity({
        position: randomEmptyPosition(world, entities, tileTypes)
      })
    )
  }

  return entities
}

export function positionTo (world, source, direction) {
  const destination = Position.to(source, direction)

  destination.x = constrain(destination.x, 0, world.width)

  return destination
}

export function randomEmptyPosition (world, entities, tileTypes) {
  const randomPos = randomPosition(world, tileTypes)

  return entities.find(e => e.position && Position.match(e.position, randomPos))
    ? randomEmptyPosition(world, entities, tileTypes)
    : randomPos
}

export function randomPosition (world, tileTypes) {
  const x = Random.int(0, world.width - 1)
  const y = Random.int(0, world.height - 1)
  const tile = world.tiles[x][y]

  return tileTypes.indexOf(tile) > -1
    ? { x, y }
    : randomPosition(world, tileTypes)
}

export function tileAt (world, { x, y }) {
  if (x < 0 || x >= world.width || y < 0 || y >= world.height) return null
  return world.tiles[x][y]
}

export function tileTo (world, direction, source) {
  return tileAt(world, positionTo(world, direction, source))
}
