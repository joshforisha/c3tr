export function constrain (number, min, max) {
  if (min >= max) throw new Error('`min` must be less than `max`')

  const span = max - min

  while (number < min) return constrain(number + span, min, max)
  while (number >= max) return constrain(number - span, min, max)
  return number
}

export function logTime (task, fn, rateFn) {
  const start = Date.now()
  const value = fn()
  const end = Date.now()
  const total = end - start

  const rateString = rateFn ? ` ${rateFn(total)}` : ''

  window.console.log(`${task} took`, total, `ms${rateString}`)
  return value
}

export function merge (a, b) {
  return Object.assign({}, a, b)
}

export function pipe (value, tasks) {
  return tasks.reduce((val, task) => task(val), value)
}
