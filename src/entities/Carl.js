import * as Avatar from '/components/Avatar'
import * as CPU from '/entities/CPU'
import * as CPUSlot from '/components/CPUSlot'
import * as Collision from '/components/Collision'
import * as Energy from '/components/Energy'
import * as Entity from '/Entity'
import * as Health from '/components/Health'
import * as ID from '/components/ID'
import * as Inventory from '/components/Inventory'
import * as MoveAction from '/components/MoveAction'
import * as MoveRestriction from '/components/MoveRestriction'
import * as Name from '/components/Name'
import * as Position from '/components/Position'
import * as Tile from '/Tile'
import * as TileType from '/data/TileType'

const Carl = 'Carl'

export function canAffordMoveTo (tile = null) {
  if (tile === null) return false
  return this.energy.current >= moveCost(tile)
}

export function create ({ position }) {
  return Entity.create(Carl, [
    Avatar.create('🤖', '#f8f8f8', 100),
    Collision.create(),
    CPUSlot.create(CPU.create({ clockSpeed: 1.0 })),
    Energy.create(60000),
    Health.create(100),
    ID.create(Carl),
    Inventory.create(),
    MoveAction.create(drainEnergyFromMoveTo),
    MoveRestriction.create(canAffordMoveTo),
    Name.create('Carl'),
    Position.create(position.x, position.y)
  ])
}

export function drainEnergyFromMoveTo (tile) {
  return Energy.drain(this, moveCost(tile))
}

export function findIn (entities) {
  return entities.find(e => e.id === Carl)
}

export function moveCost (tile) {
  return Tile.is(tile, TileType.Water) ? 100 : 10
}
