import * as Avatar from '/components/Avatar'
import * as Collision from '/components/Collision'
import * as Entity from '/Entity'
import * as Health from '/components/Health'
import * as Inventory from '/components/Inventory'
import * as Logic from '/components/Logic'
import * as LogicMode from '/data/LogicMode'
import * as Name from '/components/Name'
import * as Position from '/components/Position'

const Monster = 'Monster'

export function create ({ position }) {
  return Entity.create(Monster, [
    Avatar.create('🧟', '#e86b6b'),
    Collision.create(),
    Health.create(100),
    Inventory.create([]),
    Logic.create(LogicMode.Hostile),
    Name.create('monster'),
    Position.create(position.x, position.y)
  ])
}
