import * as Avatar from '/components/Avatar'
import * as Collision from '/components/Collision'
import * as Entity from '/Entity'
import * as Name from '/components/Name'
import * as Position from '/components/Position'

const Tree = 'Tree'

export function create ({ position }) {
  return Entity.create(Tree, [
    Avatar.create('🌳', '#573'),
    Collision.create(),
    Name.create('tree'),
    Position.create(position.x, position.y)
  ])
}
