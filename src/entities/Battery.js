import * as Avatar from '/components/Avatar'
import * as Energy from '/components/Energy'
import * as Entity from '/Entity'
import * as Name from '/components/Name'
import * as Position from '/components/Position'
import * as Random from '/Random'

const Battery = 'Battery'

export function create ({ level, position }) {
  let battery = [
    Avatar.create('🔋', '#9DD'),
    Energy.create(level || Random.int(600, 1800)),
    Name.create('battery')
  ]

  if (position) {
    battery.push(Position.create(position.x, position.y))
  }

  return Entity.create(Battery, battery)
}
