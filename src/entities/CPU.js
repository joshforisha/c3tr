import * as Avatar from '/components/Avatar'
import * as ClockSpeed from '/components/ClockSpeed'
import * as Entity from '/Entity'
import * as Name from '/components/Name'
import * as Position from '/components/Position'
import * as Random from '/Random'

const CPU = 'CPU'

export function create ({ clockSpeed, position }) {
  let cpu = Entity.create(CPU, [
    Avatar.create('¤', '#CC6'),
    ClockSpeed.create(clockSpeed),
    Name.create('CPU')
  ])

  if (position) {
    cpu = Entity.attach(cpu, Position.create(position.x, position.y))
  }

  return cpu
}

export function generate () {
  return create(Random.float(1, 6).toFixed(1))
}
