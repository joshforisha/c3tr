import * as Carl from '/entities/Carl'
import * as Entity from '/Entity'
import * as Position from '/components/Position'
import * as Tile from '/Tile'
import * as World from '/World'
import { merge, pipe } from '/Util'

export function run ({ state, world }) {
  const messages = []
  const removeEntities = []

  const entities = state.entities
    .map(entity => {
      if (!entity.move || !entity.position) return entity

      const destPos = World.positionTo(world, entity.position, entity.move)
      const destTile = World.tileAt(world, destPos)

      if (
        destTile === null ||
        !Tile.isPassable(destTile) ||
        (entity.moveRestriction && !entity.moveRestriction(destTile))
      ) {
        return Entity.detach(entity, 'move')
      }

      const destEntities = state.entities
        .filter(e => e.position)
        .filter(e => Position.match(e.position, destPos))

      if (Entity.is(entity, Carl)) {
        const collidingEntity = destEntities.find(e => e.collision)
        if (collidingEntity) {
          messages.push({
            position: destPos,
            text: `You bump into a ${collidingEntity.name}.`
          })
          return Entity.detach(entity, 'move')
        }

        if (destEntities.length > 0) {
          const entityNames = destEntities
            .filter(e => e.name)
            .map(e => `a ${e.name}`)
            .join(', ')
          messages.push({
            position: destPos,
            text: `You see ${entityNames}.`
          })
        }
      }

      return pipe(entity, [
        e => (e.moveAction ? e.moveAction(destTile) : e),
        e => Entity.update(e, { position: () => destPos }),
        e => Entity.detach(e, 'move')
      ])
    })
    .filter(entity => !removeEntities.includes(entity))

  return merge(state, {
    entities,
    messages: state.messages.concat(messages)
  })
}
