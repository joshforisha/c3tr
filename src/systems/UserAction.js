import * as Action from '/data/Action'
import * as Carl from '/entities/Carl'
import * as DOM from '/DOM'
import * as Direction from '/data/Direction'
import * as Entity from '/Entity'
import * as Move from '/components/Move'
import * as Position from '/components/Position'
import Focus from '/data/Focus'
import gameKeyBindings from '/keyBindings/game'
import inventoryKeyBindings from '/keyBindings/inventory'
import itemsKeyBindings from '/keyBindings/items'
import logKeyBindings from '/keyBindings/log'
import { merge } from '/Util'

function boundAction (bindings, keydown) {
  const binding = bindings.find(
    b => b.key.code === keydown.keyCode && b.key.shiftKey === keydown.shiftKey
  )

  return binding ? binding.action : null
}

function move (carl, direction) {
  return Entity.attach(carl, Move.create(direction))
}

export function run (model) {
  if (!model.state.keydown) return model.state

  let action
  switch (model.state.focus) {
    case Focus.Game:
      action = boundAction(gameKeyBindings, model.state.keydown)
      break

    case Focus.Inventory:
      action = boundAction(inventoryKeyBindings, model.state.keydown)
      break

    case Focus.Items:
      action = boundAction(itemsKeyBindings, model.state.keydown)
      break

    case Focus.Log:
      action = boundAction(logKeyBindings, model.state.keydown)
      break
  }

  let carl = Carl.findIn(model.state.entities)
  let focus = model.state.focus

  switch (action) {
    case Action.FocusGame:
      DOM.find('#Inventory').classList.remove('-open')
      DOM.find('#Items').classList.remove('-open')
      DOM.find('#Log').classList.remove('-open')
      focus = Focus.Game
      break

    case Action.FocusInventory:
      DOM.find('#Inventory').classList.add('-open')
      focus = Focus.Inventory
      break

    case Action.FocusItems:
      if (
        model.state.entities.filter(
          e =>
            e.position &&
            !Entity.is(e, Carl) &&
            Position.match(e.position, carl.position)
        ).length > 0
      ) {
        focus = togglePanel('Items') ? Focus.Items : Focus.Game
      }
      DOM.find('#Items').classList.add('-open')
      focus = Focus.Items
      break

    case Action.FocusLog:
      DOM.find('#Log').classList.add('-open')
      focus = Focus.Log
      break

    case Action.MoveEast:
      carl = move(carl, Direction.East)
      break

    case Action.MoveNorth:
      carl = move(carl, Direction.North)
      break

    case Action.MoveNortheast:
      carl = move(carl, Direction.Northeast)
      break

    case Action.MoveNorthwest:
      carl = move(carl, Direction.Northwest)
      break

    case Action.MoveSouth:
      carl = move(carl, Direction.South)
      break

    case Action.MoveSoutheast:
      carl = move(carl, Direction.Southeast)
      break

    case Action.MoveSouthwest:
      carl = move(carl, Direction.Southwest)
      break

    case Action.MoveWest:
      carl = move(carl, Direction.West)
      break
  }

  return merge(model.state, {
    entities: model.state.entities.map(e => (Entity.is(e, Carl) ? carl : e)),
    focus,
    keydown: null
  })
}

function togglePanel (id) {
  const panel = DOM.find(`#${id}`)

  if (panel.classList.contains('-open')) {
    panel.classList.remove('-open')
    return false
  }

  panel.classList.add('-open')
  return true
}
