import * as Carl from '/entities/Carl'
import * as DOM from '/DOM'
import * as DefaultWorld from '/worlds/Default'
import * as Focus from '/data/Focus'
import * as LogSystem from '/systems/Log'
import * as MovementSystem from '/systems/Movement'
import * as UserActionSystem from '/systems/UserAction'
import renderField from '/views/field'
import renderInventory from '/views/inventory'
import renderItems from '/views/items'
import renderMessage from '/views/message'
import renderStatus from '/views/status'
import { logTime, merge } from '/Util'

function getInitialModel () {
  const worldHeight = 768
  const worldWidth = 1024

  const world = logTime('World generation', () =>
    DefaultWorld.generate(worldWidth, worldHeight)
  )

  const entities = logTime('World population', () =>
    DefaultWorld.populate(world)
  )

  const carlPos = DefaultWorld.startingPosition(world, entities)

  return {
    state: {
      entities: entities.concat(Carl.create({ position: carlPos })),
      focus: Focus.Game,
      keydown: null,
      messages: [],
      worldHeight,
      worldWidth
    },
    world
  }
}

export function init () {
  let drawFrame = null
  let model = getInitialModel()

  function draw () {
    if (drawFrame) window.cancelAnimationFrame(drawFrame)
    drawFrame = window.requestAnimationFrame(renderViews.bind(null, model))
  }

  function handleKey (keydown) {
    window.removeEventListener('keydown', handleKey)
    model = merge(model, { state: merge(model.state, { keydown }) })
    model = update(model)
    renderViews(model)
    window.requestAnimationFrame(() => {
      window.addEventListener('keydown', handleKey)
    })
  }

  window.addEventListener('keydown', handleKey)

  window.addEventListener('resize', () => {
    resizeField(window.innerWidth, window.innerHeight)
    draw()
  })

  resizeField(window.innerWidth, window.innerHeight)
  logTime('Initial render', () => renderViews(model))
}

function renderViews (model) {
  switch (model.state.focus) {
    case Focus.Game:
      renderField(model)
      break

    case Focus.Inventory:
      renderInventory(model)
      break

    case Focus.Items:
      renderItems(model)
      break
  }

  renderMessage(model)
  renderStatus(model)
}

function resizeField (width, height) {
  const field = DOM.find('#Field')

  field.setAttribute('width', width)
  field.setAttribute('height', height - 74)
}

function update (model) {
  model = merge(model, {
    state: merge(model.state, { messages: [] })
  })
  let state = UserActionSystem.run(model)

  if (model.state.focus === Focus.Game) {
    // state = LogicSystem.run(merge(model, { state }))
    state = MovementSystem.run(merge(model, { state }))
    // state = CombatSystem.run(merge(model, { state }))
  }

  state = LogSystem.run(merge(model, { state }))

  return {
    state: merge(model.state, state),
    world: model.world
  }
}
