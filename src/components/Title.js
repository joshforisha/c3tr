import Random from '/Random'
import TitleData from '/data/Title'

export function create (title) {
  return {
    key: 'title',
    value: title
  }
}

export function generate () {
  const format = Random.from(TitleData.formats)
  const words = format.needs.map(need => Random.from(need))

  return create(format.fn(words))
}
