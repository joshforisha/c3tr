export function create(color) {
  return {
    key: 'backgroundColor',
    value: color
  };
}
