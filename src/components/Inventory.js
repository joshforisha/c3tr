import Entity from '/Entity'

export function addItem (entity, item) {
  return Entity.update(entity, {
    inventory: contents => contents.concat(item)
  })
}

export function create (contents = []) {
  return {
    key: 'inventory',
    value: contents
  }
}

export function empty (entity) {
  return Entity.update(entity, {
    inventory: () => []
  })
}

export function isEmpty (entity) {
  return entity.inventory.length === 0
}

export function removeItem (entity, item) {
  return Entity.update(entity, {
    inventory: contents => contents.filter(i => i !== item)
  })
}
