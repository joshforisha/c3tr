import Entity from '/Entity'

export function create (max) {
  return {
    key: 'energy',
    value: {
      current: max,
      max
    }
  }
}

export function drain (entity, amount) {
  return Entity.update(entity, {
    energy: value => ({
      current: Math.max(0, value.current - amount),
      max: value.max
    })
  })
}

export function fill (entity, amount) {
  return Entity.update(entity, {
    energy: value => ({
      current: Math.min(value.max, value.current + amount),
      max: value.max
    })
  })
}

export function isDrained (entity) {
  return entity.energy.current <= 0
}

export function isFull (entity) {
  return entity.energy.current >= 0
}
