import Entity from '/Entity'

export function create (cpu = null) {
  return {
    key: 'cpuSlot',
    value: { cpu }
  }
}

export function empty (entity) {
  return Entity.update(entity, {
    cpuSlot: () => ({ cpu: null })
  })
}

export function insert (entity, cpu) {
  return Entity.update(entity, {
    cpuSlot: () => ({ cpu })
  })
}

export function isEmpty (entity) {
  return entity.cpuSlot.cpu === null
}
