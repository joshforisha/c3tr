export function create(clockSpeed) {
  return {
    key: 'clockSpeed',
    value: clockSpeed
  };
}
