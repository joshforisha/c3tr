import Entity from '/Entity'

export function create (max) {
  return {
    key: 'health',
    value: {
      current: max,
      max
    }
  }
}

export function heal (entity, amount) {
  return Entity.update(entity, {
    health: value => ({
      current: Math.min(value.max, value.current + amount),
      max: value.max
    })
  })
}

export function hit (entity, amount) {
  return Entity.update(entity, {
    health: value => ({
      current: Math.max(0, value.current - amount),
      max: value.max
    })
  })
}

export function isAlive (entity) {
  return entity.health > 0
}

export function isDead (entity) {
  return !isAlive(entity)
}
