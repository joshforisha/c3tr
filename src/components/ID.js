import Random from '/Random'

export function create (id) {
  return {
    key: 'id',
    value: id
  }
}

export function generate () {
  return create(`${Date.now()}${Random.integer(0, 999)}`)
}
