import Entity from '/Entity'
import Offset from '/data/Offset'

export function create (x, y) {
  return {
    key: 'position',
    value: { x, y }
  }
}

export function match (a, b) {
  return a.x === b.x && a.y === b.y
}

export function set (entity, { x, y }) {
  return Entity.update(entity, {
    position: () => ({ x, y })
  })
}

export function to (source, direction) {
  const offset = Offset[direction]

  return {
    x: source.x + offset.x,
    y: source.y + offset.y
  }
}
