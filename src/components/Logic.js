import LogicMode from '/data/LogicMode'

export function create (mode = LogicMode.PASSIVE) {
  return {
    key: 'logic',
    value: mode
  }
}
