const adjectives = [
  'Autonomous',
  'First',
  'Giant',
  'Humanoid',
  'Industrial',
  'Intelligent',
  'Little',
  'Mechanical',
  'Mobile',
  'New',
  'Small'
]

export const formats = [
  {
    needs: [adjectives, nouns],
    fn: () => {} // fmt`${0} ${1}`
  }
]

export const nouns = [
  'Ball Bearing',
  'Bearing',
  'Belt',
  'Cog',
  'Camshaft',
  'Component',
  'Controller',
  'Crank',
  'Module',
  'Motor',
  'Pinion',
  'Seal',
  'Sprocket',
  'Valve'
]
