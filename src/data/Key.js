export default {
  Comma: { code: 188, shiftKey: false },
  Down: { code: 40, shiftKey: false },
  Enter: { code: 13, shiftKey: false },
  Escape: { code: 27, shiftKey: false },
  Left: { code: 37, shiftKey: false },
  LowerA: { code: 65, shiftKey: false },
  LowerB: { code: 66, shiftKey: false },
  LowerC: { code: 67, shiftKey: false },
  LowerD: { code: 68, shiftKey: false },
  LowerE: { code: 69, shiftKey: false },
  LowerF: { code: 70, shiftKey: false },
  LowerG: { code: 71, shiftKey: false },
  LowerH: { code: 72, shiftKey: false },
  LowerI: { code: 73, shiftKey: false },
  LowerJ: { code: 74, shiftKey: false },
  LowerK: { code: 75, shiftKey: false },
  LowerL: { code: 76, shiftKey: false },
  LowerM: { code: 77, shiftKey: false },
  LowerN: { code: 78, shiftKey: false },
  LowerO: { code: 79, shiftKey: false },
  LowerP: { code: 80, shiftKey: false },
  LowerQ: { code: 81, shiftKey: false },
  LowerR: { code: 82, shiftKey: false },
  LowerS: { code: 83, shiftKey: false },
  LowerT: { code: 84, shiftKey: false },
  LowerU: { code: 85, shiftKey: false },
  LowerV: { code: 86, shiftKey: false },
  LowerW: { code: 87, shiftKey: false },
  LowerX: { code: 88, shiftKey: false },
  LowerY: { code: 89, shiftKey: false },
  LowerZ: { code: 90, shiftKey: false },
  QuestionMark: { code: 191, shiftKey: true },
  Right: { code: 39, shiftKey: false },
  Up: { code: 38, shiftKey: false },
  UpperA: { code: 65, shiftKey: true },
  UpperB: { code: 66, shiftKey: true },
  UpperC: { code: 67, shiftKey: true },
  UpperD: { code: 68, shiftKey: true },
  UpperE: { code: 69, shiftKey: true },
  UpperF: { code: 70, shiftKey: true },
  UpperG: { code: 71, shiftKey: true },
  UpperH: { code: 72, shiftKey: true },
  UpperI: { code: 73, shiftKey: true },
  UpperJ: { code: 74, shiftKey: true },
  UpperK: { code: 75, shiftKey: true },
  UpperL: { code: 76, shiftKey: true },
  UpperM: { code: 77, shiftKey: true },
  UpperN: { code: 78, shiftKey: true },
  UpperO: { code: 79, shiftKey: true },
  UpperP: { code: 80, shiftKey: true },
  UpperQ: { code: 81, shiftKey: true },
  UpperR: { code: 82, shiftKey: true },
  UpperS: { code: 83, shiftKey: true },
  UpperT: { code: 84, shiftKey: true },
  UpperU: { code: 85, shiftKey: true },
  UpperV: { code: 86, shiftKey: true },
  UpperW: { code: 87, shiftKey: true },
  UpperX: { code: 88, shiftKey: true },
  UpperY: { code: 89, shiftKey: true },
  UpperZ: { code: 90, shiftKey: true }
}
