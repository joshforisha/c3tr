export default {
  East: 'EAST',
  North: 'NORTH',
  Northeast: 'NORTHEAST',
  Northwest: 'NORTHWEST',
  South: 'SOUTH',
  Southeast: 'SOUTHEAST',
  Southwest: 'SOUTHWEST',
  West: 'WEST'
}
