export default {
  East: { x: +1, y: 0 },
  North: { x: 0, y: -1 },
  Northeast: { x: +1, y: -1 },
  Northwest: { x: -1, y: -1 },
  South: { x: 0, y: +1 },
  Southeast: { x: +1, y: +1 },
  Southwest: { x: -1, y: +1 },
  West: { x: -1, y: 0 }
}
