export function entitiesAt (model, position) {
  return model.state.entities
    .filter(entity => entity.hasOwnProperty('position'))
    .filter(
      entity =>
        entity.position.x === position.x && entity.position.y === position.y
    )
}

export function find ({ state }, matchEntity) {
  return state.entities.find(matchEntity)
}
